package com.veritran.CliAplication.util;

public class CliAplicationConstans {
	
	public static final String AD_USER = "1";
	public static final String DEPOSITE = "2";
	public static final String EXTRACT = "3";
	public static final String TRANSFER = "4";
	public static final String BALANCE = "5";
	public static final String OPERATIONS_HISTORY = "6";
	public static final String EXIT = "7";
	public static final String QUESTION_CONTNUE = "Do you want to try again?";
	public static final String QUESTION_OTHER_OPERATION="Do you want to perform another operation?";
	
	public static final String ERROR_FORMAT =" Invalid format. Please enter the data again";
	
	

}
