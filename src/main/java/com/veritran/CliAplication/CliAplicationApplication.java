package com.veritran.CliAplication;


import java.util.InputMismatchException;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.veritran.BankSystem.config.AplicationConfig;
import com.veritran.BankSystem.dto.UserDto;
import com.veritran.BankSystem.entities.Account;
import com.veritran.BankSystem.entities.Operation;
import com.veritran.BankSystem.service.AccountService;
import com.veritran.BankSystem.service.ConverterService;
import com.veritran.BankSystem.service.UserService;
import com.veritran.BankSystem.service.impl.AccountServiceImpl;
import com.veritran.BankSystem.service.impl.ConverterServiceImpl;
import com.veritran.BankSystem.service.impl.UserServiceImpl;
import com.veritran.BankSystem.util.Constants;
import com.veritran.BankSystem.util.CustomException;
import com.veritran.CliAplication.util.CliAplicationConstans;

@ComponentScan("com.veritran.BankSystem.service")
@EnableJpaRepositories("com.veritran.BankSystem.repository")
@EntityScan("com.veritran.BankSystem.entities")
@Configuration
@Import(AplicationConfig.class)
@SpringBootApplication
public class CliAplicationApplication {

	private static final org.slf4j.Logger LOG =  LoggerFactory.getLogger(CliAplicationApplication.class);

	public static  Scanner sc;
	public static UserService userService;
	public static AccountService accountService;
	public static ConverterService converterService;
	public static int option;
	public static boolean repeat;
	
	
	public static void main(String[] args) throws CustomException {
		ConfigurableApplicationContext context = SpringApplication.run(CliAplicationApplication.class, args);
		 userService = context.getBean(UserServiceImpl.class);
		 accountService = context.getBean(AccountServiceImpl.class);
		 converterService = context.getBean(ConverterServiceImpl.class);
		
			operation(); 
		}
		
	
	
	public static void operation() throws InputMismatchException{
		sc = new Scanner(System.in);
        
        do {
		System.out.println("Select Operation");
		System.out.println("1. ADD_USER");
		System.out.println("2. DEPOSITE");
		System.out.println("3. EXTRACT");
		System.out.println("4. TRANSFER");
		System.out.println("5. BALANCE");
		System.out.println("6. OPERATIONS HISTORY");
		System.out.println("7. EXIT");
        
        String operation = sc.nextLine();
        
        switch (operation) {
        
		case CliAplicationConstans.AD_USER:
			addUser();
			break;
		case CliAplicationConstans.DEPOSITE:
			deposit();
			break;
		case CliAplicationConstans.EXTRACT:
			extract();
			break;
		case CliAplicationConstans.TRANSFER:
			transfer();
		break;
		case CliAplicationConstans.BALANCE:
			balance();
		break;
		case CliAplicationConstans.OPERATIONS_HISTORY:
			getOperations();
		break;
		case CliAplicationConstans.EXIT:
			LOG.info("EXIT");
			System.exit(0);
		break;
		default:
			operation();
		}
        
        
        repeat = questionContinue(CliAplicationConstans.QUESTION_OTHER_OPERATION);
        
        }while(repeat);
        
        System.exit(0);
	}


	public static void addUser() {
		repeat =false;
		do {
			try {
				System.out.println("please enter your username");
		        String userName = sc.nextLine();
				System.out.println("please enter your name");
		        String name = sc.nextLine();
		        System.out.println("please enter your surname");
		        String surname = sc.nextLine();
		        System.out.println("please enter your dni");
		        String dni = sc.nextLine();
		        System.out.println("please enter your email");
		        String email = sc.nextLine();
		        System.out.println("please select a password");
		        String password = sc.nextLine();
		        
		        UserDto userDto = new UserDto(name,surname,userName,email,dni,password);
		        userService.addUserAndAccount(userDto);
		        repeat=false;
			} catch (InputMismatchException e) {
				System.out.println(CliAplicationConstans.ERROR_FORMAT);
				sc.nextLine();
				repeat=true;
			}catch (Exception e) {
				LOG.error(e.getMessage());
				repeat=questionContinue(CliAplicationConstans.QUESTION_CONTNUE);
				sc.nextLine();
			}
		}while(repeat);
		
	}
	
	
	public static void deposit() {
		repeat =false;
		do {	
			try {
				UserDto userDto = getUser();
				if(userDto.getIdUser()!=null) {
				System.out.println("Insert money to deposit");
				Double money = sc.nextDouble();
				System.out.println("Insert comments");
				String comments = sc.nextLine();			
				userService.deposit(userDto.getIdUser(), money, Constants.DEPOSITE, comments);
				repeat=false;
				}else {
				       repeat = questionContinue(CliAplicationConstans.QUESTION_CONTNUE);
				       sc.nextLine();
				}
			} catch (InputMismatchException e) {
				System.out.println(CliAplicationConstans.ERROR_FORMAT);
				repeat = true;
				sc.nextLine();
			} catch (CustomException e) {
				System.out.println(e.getMessage());
				repeat = questionContinue(CliAplicationConstans.QUESTION_CONTNUE);
				sc.nextLine();
			}catch(Exception e){
				LOG.error(e.getMessage());
				repeat = questionContinue(CliAplicationConstans.QUESTION_CONTNUE);
				sc.nextLine();
			}
		}while(repeat);	
		
	}
	
	public static void extract()  {
		repeat=false;
		do {
			try {
				UserDto userDto = getUser();
				if(userDto.getIdUser()!=null) {
				System.out.println("Insert money to extract");
				Double money = sc.nextDouble();
				System.out.println("Insert comments");
				sc.next();
				String comments = sc.nextLine();
				userService.extract(userDto.getIdUser(), money, Constants.EXTRACT, comments);
				repeat=false;
				}else {
				       repeat = questionContinue(CliAplicationConstans.QUESTION_CONTNUE);
				       sc.nextLine();
				}
			} catch(InputMismatchException e){
				System.out.println(CliAplicationConstans.ERROR_FORMAT);
				repeat = true;
			}catch (CustomException e) {
				System.out.println(e.getMessage());
				repeat = questionContinue(CliAplicationConstans.QUESTION_CONTNUE);
			}catch (Exception e) {
				LOG.error(e.getMessage());
				repeat = questionContinue(CliAplicationConstans.QUESTION_CONTNUE);
				sc.nextLine();
			}
		}while(repeat);
	}
	
	public static void transfer(){
		repeat=false;
		do {
			try {
				System.out.println("Choose User");
				UserDto userDtoGives = getUser();
				System.out.println("Choose target User");
				UserDto targetUserDto=getUser();
				System.out.println("Insert money to transfer");
				Double money = sc.nextDouble();
				System.out.println("Insert comments");
				String comments = sc.nextLine();
				if(userDtoGives.getIdUser()!=null&&targetUserDto.getIdUser()!=null) {
					userService.transfer(userDtoGives.getIdUser(), targetUserDto.getIdUser(), money, comments);
					repeat=false;
					}else {
						repeat=questionContinue(CliAplicationConstans.QUESTION_CONTNUE);
						sc.nextLine();
				}
			} catch(InputMismatchException e){
				System.out.println(CliAplicationConstans.ERROR_FORMAT);
				sc.nextLine();
				repeat=true;
				
			}catch(CustomException e){
				System.out.println(e.getMessage());
				repeat=questionContinue(CliAplicationConstans.QUESTION_CONTNUE);
				sc.nextLine();
			}catch (Exception e) {
				LOG.error(e.getMessage());
				repeat = questionContinue(CliAplicationConstans.QUESTION_CONTNUE);
				sc.nextLine();
			}
		}while(repeat);	
	}
	
	public static UserDto getUser() {
		System.out.println("Insert  userName");
		String userName =  sc.nextLine();
		System.out.println("Insert  dni");
		String dni = sc.nextLine();
		UserDto userDtoResult = new UserDto();
		try {
			 userDtoResult = userService.getByUserNameAndDni(userName, dni);
		} catch (CustomException e) {
			System.out.println(e.getMessage());
		}
		return userDtoResult;
	}
	
	public static void balance() {
		repeat=false;
		do {
			UserDto userDtoResult = getUser();
			if(userDtoResult.getIdUser()!=null) {
				Optional<Account> optionalAccount= accountService.getAccountById(userDtoResult.getIdUser());
				if (optionalAccount.isPresent()) {
				System.out.println("The balance of "+ userDtoResult.getUserName()+" is "+optionalAccount.get().getBalance());
				repeat=false;
				}else {
					LOG.error(Constants.ACCOUNT_NOT_FOUND );
			}
			}else {
				repeat= questionContinue(CliAplicationConstans.QUESTION_CONTNUE);
				sc.nextLine();
			}
		}while(repeat);
		
		
	}
	
	public static void getOperations() {
		repeat=false;
		do {
			UserDto userDtoResult = getUser();
			if(userDtoResult.getIdUser()!=null) {
				Optional<Account> optionalAccount= accountService.getAccountById(userDtoResult.getIdUser());
				if (optionalAccount.isPresent()) {
				System.out.println("Operations history of "+ userDtoResult.getUserName()+ " is");
		        List<Operation> operations = optionalAccount.get().getListOperation();
				for(Operation operation : operations) {
		            System.out.println(operation.getType()+" amount "+operation.getAmount()+" "+operation.getDescription());
		        }	
				repeat=false;
				}else {
					LOG.error(Constants.ACCOUNT_NOT_FOUND );
				}
			}else {
				repeat=questionContinue(CliAplicationConstans.QUESTION_CONTNUE);
				sc.nextLine();
			}
		}while(repeat);
	}
	
	public static boolean questionContinue(String question) {
		boolean result=false;
		System.out.println(question);
		System.out.println("0. no");
        System.out.println("1. yes");
        
		option = sc.nextInt();
		
		while(option!=0 && option!=1)
        {
        	System.out.println("Please select a valid option");
        	System.out.println("0. no");
            System.out.println("1. yes");
        	option = sc.nextInt();
        }
        if(option==1) {
        	result=true;
        }else {
        	result=false;
        }
        return result;
        
	} 

}
